package com.uk.androidrecruitmentapp

import androidx.lifecycle.ViewModelProviders
import com.uk.androidrecruitmentapp.di.ApiModule
import com.uk.androidrecruitmentapp.di.DaggerApiComponent
import com.uk.androidrecruitmentapp.ui.fragments.CharactersFragment
import com.uk.androidrecruitmentapp.ui.viewmodels.CharactersViewModel
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.CharactersViewModelFactory
import org.junit.Test
import javax.inject.Inject

class CharactersTest {


    @Inject
    lateinit var viewModelFactory: CharactersViewModelFactory

    lateinit var viewModel: CharactersViewModel

    @Test
    fun downloadChars(){
        ARApplication.apiComponent = DaggerApiComponent.builder()
                .apiModule(ApiModule())
                .build()
        val fragment = CharactersFragment()
        ARApplication.apiComponent.inject(fragment)
        viewModel = ViewModelProviders.of(fragment,viewModelFactory).get(CharactersViewModel::class.java)

    }
}