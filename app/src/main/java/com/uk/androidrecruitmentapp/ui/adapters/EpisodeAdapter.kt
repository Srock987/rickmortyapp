package com.uk.androidrecruitmentapp.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.uk.androidrecruitmentapp.R
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodeResult
import com.uk.androidrecruitmentapp.ui.adapters.diffcalback.EpisodeDiffCallback
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_episode.*

class EpisodeAdapter(items: List<EpisodeResult>, listener: SlidingListener) : BaseAdapter<EpisodeResult>(items, listener) {

    override fun calculateDiffResult(oldList: List<EpisodeResult>, newList: List<EpisodeResult>): DiffUtil.DiffResult {
        val callback = EpisodeDiffCallback(oldList,newList)
        return DiffUtil.calculateDiff(callback)
    }

    override fun getLayoutId(position: Int, obj: EpisodeResult): Int {
        return R.layout.view_episode
    }

    override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
        return EpisodeViewHolder(view)
    }

    class EpisodeViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer, BaseAdapter.Binder<EpisodeResult>{
        override fun bind(data: EpisodeResult) {
            episodeName.text = data.name
            episodeNumbering.text=data.episode
            airDate.text=data.air_date
        }

    }
}