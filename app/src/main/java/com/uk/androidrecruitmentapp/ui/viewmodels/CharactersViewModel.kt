package com.uk.androidrecruitmentapp.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log
import com.uk.androidrecruitmentapp.data.local.characters.Characters
import com.uk.androidrecruitmentapp.data.local.characters.CharactersData
import com.uk.androidrecruitmentapp.data.local.characters.CharactersExhaustionError
import com.uk.androidrecruitmentapp.data.local.characters.CharactersUnavailableError
import com.uk.androidrecruitmentapp.data.repositories.CharactersRepository
import com.uk.androidrecruitmentapp.data.utils.DataState
import com.uk.androidrecruitmentapp.utils.NextInfoExtractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CharactersViewModel(repository: CharactersRepository) : BaseViewModel<Characters>(repository) {

    override fun dataExhausted() {
        updateData(CharactersUnavailableError("No more characters"))
    }

    override fun dataUnavailable() {
        updateData(CharactersUnavailableError("Can't connect"))
    }

    override fun dataReceived(data: Characters) {
        if (data is CharactersData) {
            val currentList = getResultList().value
            when (currentList) {
                is CharactersData -> updateData(CharactersData(data.info,currentList.results.plus(data.results.filter { t -> !currentList.results.contains(t) })))
                is CharactersExhaustionError -> updateData(currentList)
                is CharactersUnavailableError -> updateData(currentList)
                null -> updateData(data)
            }
            checkExhaustion(data.info!!)
        }
    }

}