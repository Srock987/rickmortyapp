package com.uk.androidrecruitmentapp.ui.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.util.Log
import com.uk.androidrecruitmentapp.ARApplication
import com.uk.androidrecruitmentapp.data.local.locations.LocationData
import com.uk.androidrecruitmentapp.data.local.locations.LocationExhaustionError
import com.uk.androidrecruitmentapp.data.local.locations.LocationUnavailableError
import com.uk.androidrecruitmentapp.data.utils.DataState
import com.uk.androidrecruitmentapp.ui.adapters.LocationsAdapter
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import com.uk.androidrecruitmentapp.ui.viewmodels.LocationsViewModel
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.LocationsViewModelFactory
import kotlinx.android.synthetic.main.list_fragment.*
import javax.inject.Inject

class LocationsFragment : BaseFragment(), SlidingListener {

    @Inject
    lateinit var viewModelFactory: LocationsViewModelFactory

    lateinit var viewModel: LocationsViewModel

    private var adapter: LocationsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ARApplication.apiComponent.inject(this)
        adapter = LocationsAdapter(listOf(),this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(LocationsViewModel::class.java)
        contentRecyclerView.adapter=adapter
        viewModel.getResultList()
                .observe(this, Observer {
                    when(it){
                        is LocationData -> {
                            adapter?.updateData(it.results)
                            contentReady()
                        }
                        is LocationExhaustionError -> {
                            Snackbar.make(contentRecyclerView,it.errorMessage, Snackbar.LENGTH_SHORT).show()
                            adapter?.exhaustionReached()
                        }
                        is LocationUnavailableError -> Snackbar.make(contentRecyclerView,it.errorMessage, Snackbar.LENGTH_SHORT).show()
                    }
                })
    }

    override fun fetchData() {
        viewModel.getMoreData()
    }

    override fun onBottomReached() {
        viewModel.getMoreData()
    }


}