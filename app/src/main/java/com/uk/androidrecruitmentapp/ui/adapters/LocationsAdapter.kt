package com.uk.androidrecruitmentapp.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.uk.androidrecruitmentapp.R
import com.uk.androidrecruitmentapp.data.local.locations.LocationResult
import com.uk.androidrecruitmentapp.ui.adapters.diffcalback.LocationDiffCallback
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_location.*

class LocationsAdapter(items: List<LocationResult>, listener: SlidingListener) : BaseAdapter<LocationResult>(items, listener) {

    override fun calculateDiffResult(oldList: List<LocationResult>, newList: List<LocationResult>): DiffUtil.DiffResult {
        val callback = LocationDiffCallback(oldList,newList)
        return DiffUtil.calculateDiff(callback)
    }

    override fun getLayoutId(position: Int, obj: LocationResult): Int {
        return R.layout.view_location
    }

    override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
        return LocationViewHolder(view)
    }

    class LocationViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer, BaseAdapter.Binder<LocationResult>{
        override fun bind(data: LocationResult) {
            locationName.text = data.name
            locationType.text = data.type
            locationDimension.text = data.dimension
        }

    }
}