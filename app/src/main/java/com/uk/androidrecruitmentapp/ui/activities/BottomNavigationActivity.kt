package com.uk.androidrecruitmentapp.ui.activities


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import com.uk.androidrecruitmentapp.R
import com.uk.androidrecruitmentapp.ui.fragments.CharactersFragment
import com.uk.androidrecruitmentapp.ui.fragments.EpisodeFragment
import com.uk.androidrecruitmentapp.ui.fragments.LocationsFragment
import com.uk.androidrecruitmentapp.utils.NavigationType
import kotlinx.android.synthetic.main.main_activity.*


class BottomNavigationActivity : AppCompatActivity() {

    companion object {
        const val isFragmentSaved = "isSaved"
        const val pickedNavigator = "pickedNavigator"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        initNavigation()
        val fragmentSaved: Boolean
        if (savedInstanceState == null){
            fragmentSaved = false
        } else{
            fragmentSaved= savedInstanceState.getBoolean(isFragmentSaved)
            bottomNavigation.selectedItemId=savedInstanceState.getInt(pickedNavigator)
        }
        if (!fragmentSaved){
            showFragment(EpisodeFragment())
        }

    }

    private fun initNavigation(){
        bottomNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.action_characters ->showTab(NavigationType.CHARACTERS)
                R.id.action_episodes -> showTab(NavigationType.EPISODES)
                R.id.action_location -> showTab(NavigationType.LOCATIONS)
                else -> showTab(NavigationType.EPISODES)
            }
        }
    }

    private fun showTab(type: NavigationType): Boolean{
        showFragment(provideFragment(type))
        return true
    }

    private fun provideFragment(type: NavigationType):Fragment{
        when(type){
            NavigationType.EPISODES -> {
                val oldEpisodeFragment = supportFragmentManager.findFragmentByTag(EpisodeFragment::class.simpleName)
                return if (oldEpisodeFragment==null){
                    EpisodeFragment()
                } else{
                    oldEpisodeFragment
                }
            }
            NavigationType.CHARACTERS -> {
                val oldCharactersFragment = supportFragmentManager.findFragmentByTag(CharactersFragment::class.simpleName)
                return if (oldCharactersFragment==null){
                    CharactersFragment()
                } else{
                    oldCharactersFragment
                }
            }
            NavigationType.LOCATIONS -> {
                val oldLocationsFragment = supportFragmentManager.findFragmentByTag(LocationsFragment::class.simpleName)
                return if (oldLocationsFragment==null){
                    LocationsFragment()
                } else{
                    oldLocationsFragment
                }
            }
        }
    }

    private fun showFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
                .replace(R.id.content,fragment,fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(isFragmentSaved,true)
        outState?.putInt(pickedNavigator,bottomNavigation.selectedItemId)
    }


}