package com.uk.androidrecruitmentapp.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.uk.androidrecruitmentapp.R
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import kotlinx.android.extensions.LayoutContainer

abstract class BaseAdapter<T>(private var items: List<T>, private val slidingListener: SlidingListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var progressNeeded: Boolean = false
    var exhaustionReached: Boolean = false

    abstract fun calculateDiffResult(oldList: List<T>,newList: List<T>) : DiffUtil.DiffResult

    fun updateData(newList: List<T>){
        val diffResult = calculateDiffResult(items,newList)
        items=newList
        diffResult.dispatchUpdatesTo(this)
        progressNeeded = false
    }

    fun getCurrentItems() : ArrayList<T> {
        return ArrayList(items)
    }

    fun exhaustionReached(){
        exhaustionReached = true
        progressNeeded = false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if(viewType==R.layout.view_progress){
            return ProgressViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
        }
        return getViewHolder(LayoutInflater.from(parent.context)
                .inflate(viewType, parent, false), viewType)
    }

    override fun getItemCount(): Int {
        if (progressNeeded){
            return items.size + 1
        } else return items.size
    }

    override fun getItemViewType(position: Int): Int {
        if (progressNeeded.and(position + 1 == items.size)){
            return R.layout.view_progress
        } else return getLayoutId(position, items[position])
    }

    protected abstract fun getLayoutId(position: Int, obj: T): Int

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder::class) {
            ProgressViewHolder::class-> Log.d("ProgressViewHolder","Shown")
            else -> (holder as Binder<T>).bind(items[position])
        }
        if ((position == items.size - 1).and(!exhaustionReached)) {
            progressNeeded=true
            slidingListener.onBottomReached()
            Log.d("Adapter","Bottom reaced")
        }
    }

    abstract fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder

    internal interface Binder<T> {
        fun bind(data: T)
    }

    class ProgressViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView!!), LayoutContainer

}