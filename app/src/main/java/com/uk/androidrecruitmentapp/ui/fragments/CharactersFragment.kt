package com.uk.androidrecruitmentapp.ui.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.util.Log
import com.uk.androidrecruitmentapp.ARApplication
import com.uk.androidrecruitmentapp.data.local.characters.CharactersData
import com.uk.androidrecruitmentapp.data.local.characters.CharactersExhaustionError
import com.uk.androidrecruitmentapp.data.local.characters.CharactersUnavailableError
import com.uk.androidrecruitmentapp.data.utils.DataState
import com.uk.androidrecruitmentapp.ui.adapters.CharactersAdapter
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import com.uk.androidrecruitmentapp.ui.viewmodels.CharactersViewModel
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.CharactersViewModelFactory
import kotlinx.android.synthetic.main.list_fragment.*
import javax.inject.Inject

class CharactersFragment : BaseFragment(), SlidingListener {

    @Inject
    lateinit var viewModelFactory: CharactersViewModelFactory

    lateinit var viewModel: CharactersViewModel

    private var adapter: CharactersAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ARApplication.apiComponent.inject(this)
        adapter = CharactersAdapter(listOf(),this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(CharactersViewModel::class.java)
    }

    override fun onStart() {
        contentRecyclerView.adapter=adapter
        viewModel.getResultList()
                .observe(this, Observer {
                    when (it) {
                        is CharactersData -> {
                            adapter?.updateData(it.results)
                            contentReady()
                        }
                        is CharactersExhaustionError -> {
                            Snackbar.make(contentRecyclerView, it.errorMessage, Snackbar.LENGTH_SHORT).show()
                            adapter?.exhaustionReached()
                        }
                        is CharactersUnavailableError -> Snackbar.make(contentRecyclerView, it.errorMessage, Snackbar.LENGTH_SHORT).show()
                    }
                })
        super.onStart()
    }

    override fun fetchData() {
        viewModel.getMoreData()
    }

    override fun onBottomReached() {
        viewModel.getMoreData()
    }

}