package com.uk.androidrecruitmentapp.ui.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.uk.androidrecruitmentapp.data.remote.ApiService
import com.uk.androidrecruitmentapp.data.repositories.EpisodeRepository
import com.uk.androidrecruitmentapp.ui.viewmodels.EpisodeViewModel

class EpisodeViewModelFactory(private val repository: EpisodeRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EpisodeViewModel(repository) as T
    }
}