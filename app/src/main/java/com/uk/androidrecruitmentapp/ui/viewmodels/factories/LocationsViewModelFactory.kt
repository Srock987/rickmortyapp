package com.uk.androidrecruitmentapp.ui.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.uk.androidrecruitmentapp.data.remote.ApiService
import com.uk.androidrecruitmentapp.data.repositories.LocationRepository
import com.uk.androidrecruitmentapp.ui.viewmodels.LocationsViewModel

class LocationsViewModelFactory(private val repository: LocationRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LocationsViewModel(repository) as T
    }
}