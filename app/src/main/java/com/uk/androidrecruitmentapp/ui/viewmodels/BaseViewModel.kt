package com.uk.androidrecruitmentapp.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.os.Debug
import android.util.Log
import com.uk.androidrecruitmentapp.data.local.Info
import com.uk.androidrecruitmentapp.data.repositories.BaseRepository
import com.uk.androidrecruitmentapp.utils.NextInfoExtractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BaseViewModel<T>(private val repository: BaseRepository<T>) : ViewModel() {

    private lateinit var resultList: MutableLiveData<T>
    private val disposables = CompositeDisposable()
    private var exhaustion: Boolean
    private var pageCounter: Int

    init {
        pageCounter = 1
        exhaustion = false
    }

    fun getResultList(): MutableLiveData<T>{
        if (!::resultList.isInitialized){
            resultList = MutableLiveData()
        }
        return resultList
    }

    fun updateData(newData: T){
        resultList.value=newData
    }

    abstract fun dataExhausted()
    abstract fun dataUnavailable()
    abstract fun dataReceived(data: T)

    fun checkExhaustion(info: Info){
        if (NextInfoExtractor.shouldShowNextPage(info.next, info.pages)) {
            pageCounter = NextInfoExtractor.extractPage(info.next)
        } else {
            exhaustion = true
        }
    }

    fun getMoreData(){
        if (exhaustion){
            dataExhausted()
        }else {
            disposables.add(repository.getData(pageCounter)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        dataReceived(it)
                    }, {
                        dataUnavailable()
                        it.printStackTrace()
                    }))
        }
    }



}