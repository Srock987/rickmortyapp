package com.uk.androidrecruitmentapp.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.uk.androidrecruitmentapp.data.local.episodes.*
import com.uk.androidrecruitmentapp.data.repositories.EpisodeRepository
import com.uk.androidrecruitmentapp.utils.NextInfoExtractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class EpisodeViewModel(repository: EpisodeRepository) : BaseViewModel<Episodes>(repository) {

    override fun dataExhausted() {
        updateData(EpisodeExhaustionError("No more episodes"))
    }

    override fun dataUnavailable() {
        updateData(EpisodeUnavailableError("Can't connect"))
    }

    override fun dataReceived(data: Episodes) {
        if (data is EpisodesData) {
            val currentList = getResultList().value
            when (currentList) {
                is EpisodesData -> updateData(EpisodesData(data.info,currentList.results.plus(data.results.filter { t -> !currentList.results.contains(t) })))
                is EpisodeExhaustionError -> updateData(currentList)
                is EpisodeUnavailableError -> updateData(currentList)
                null -> updateData(data)
            }
            checkExhaustion(data.info!!)
        }
    }

    fun setEpisdesList(){

    }

}