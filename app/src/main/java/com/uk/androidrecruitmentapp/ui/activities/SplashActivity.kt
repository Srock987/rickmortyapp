package com.uk.androidrecruitmentapp.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.uk.androidrecruitmentapp.R

class SplashActivity : Activity() {

    private val splashDelay = 2000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            startActivity(Intent(this, BottomNavigationActivity::class.java))
            finish()
        },splashDelay)
    }
}