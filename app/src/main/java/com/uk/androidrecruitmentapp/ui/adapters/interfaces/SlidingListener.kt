package com.uk.androidrecruitmentapp.ui.adapters.interfaces

interface SlidingListener {
    fun onBottomReached()
}