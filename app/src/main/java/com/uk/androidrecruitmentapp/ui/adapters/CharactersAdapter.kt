package com.uk.androidrecruitmentapp.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.uk.androidrecruitmentapp.R
import com.uk.androidrecruitmentapp.data.local.characters.CharacterResult
import com.uk.androidrecruitmentapp.ui.adapters.diffcalback.CharactersDiffCallback
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_character.*

class CharactersAdapter(items: List<CharacterResult>, listener: SlidingListener) : BaseAdapter<CharacterResult>(items, listener) {

    override fun calculateDiffResult(oldList: List<CharacterResult>, newList: List<CharacterResult>): DiffUtil.DiffResult {
        val callback = CharactersDiffCallback(oldList,newList)
        return DiffUtil.calculateDiff(callback)
    }

    override fun getLayoutId(position: Int, obj: CharacterResult): Int {
        return R.layout.view_character
    }

    override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
        return CharacterViewHolder(view)
    }

    class CharacterViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer, BaseAdapter.Binder<CharacterResult>{
        override fun bind(data: CharacterResult) {
            charName.text = data.name
            charOrigin.text = data.origin.name
            charGender.text = data.gender
            charStatus.text = data.status
            charSpecies.text = data.species
            Glide.with(containerView)
                    .load(data.image)
                    .into(charImageView)
        }
    }
}