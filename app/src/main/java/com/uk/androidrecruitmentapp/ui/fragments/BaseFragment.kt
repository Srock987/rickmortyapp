package com.uk.androidrecruitmentapp.ui.fragments


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.transition.Visibility
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.uk.androidrecruitmentapp.R
import com.uk.androidrecruitmentapp.ui.viewmodels.BaseViewModel
import com.uk.androidrecruitmentapp.ui.viewmodels.EpisodeViewModel
import kotlinx.android.synthetic.main.list_fragment.*
import kotlinx.android.synthetic.main.list_fragment.view.*

abstract class BaseFragment : Fragment() {

    companion object {
        const val DATA_LOADED = "DATA_LOADED"
    }

    var dataLoaded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState!=null){
            dataLoaded= savedInstanceState.getBoolean(DATA_LOADED)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_fragment,container,false)
    }

    fun contentReady(){
        progressIndicator.visibility=View.GONE
        contentRecyclerView.visibility=View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        contentRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        if (!dataLoaded) {
            fetchData()
            dataLoaded = true
        }
    }

    abstract fun fetchData()

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(DATA_LOADED,dataLoaded)
    }




}