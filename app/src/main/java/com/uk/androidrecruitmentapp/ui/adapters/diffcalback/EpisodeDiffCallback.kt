package com.uk.androidrecruitmentapp.ui.adapters.diffcalback

import androidx.recyclerview.widget.DiffUtil
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodeResult
import com.uk.androidrecruitmentapp.data.local.locations.LocationResult

class EpisodeDiffCallback(private val oldList: List<EpisodeResult>, private val newList: List<EpisodeResult>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList.get(oldItemPosition).id == newList.get(newItemPosition).id
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition))
    }

}