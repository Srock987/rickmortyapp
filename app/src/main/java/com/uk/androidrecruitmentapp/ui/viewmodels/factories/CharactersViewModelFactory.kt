package com.uk.androidrecruitmentapp.ui.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.uk.androidrecruitmentapp.data.remote.ApiService
import com.uk.androidrecruitmentapp.data.repositories.CharactersRepository
import com.uk.androidrecruitmentapp.ui.viewmodels.CharactersViewModel
import com.uk.androidrecruitmentapp.ui.viewmodels.EpisodeViewModel

class CharactersViewModelFactory(private val repository: CharactersRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CharactersViewModel(repository) as T
    }
}