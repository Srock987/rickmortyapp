package com.uk.androidrecruitmentapp.ui.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.util.Log
import com.uk.androidrecruitmentapp.ARApplication
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodesData
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodeExhaustionError
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodeUnavailableError
import com.uk.androidrecruitmentapp.ui.adapters.EpisodeAdapter
import com.uk.androidrecruitmentapp.ui.adapters.interfaces.SlidingListener
import com.uk.androidrecruitmentapp.ui.viewmodels.EpisodeViewModel
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.EpisodeViewModelFactory
import kotlinx.android.synthetic.main.list_fragment.*
import javax.inject.Inject

class EpisodeFragment : BaseFragment(), SlidingListener {

    @Inject
    lateinit var viewModelFactory: EpisodeViewModelFactory

    lateinit var viewModel: EpisodeViewModel

    private var adapter: EpisodeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ARApplication.apiComponent.inject(this)
        adapter = EpisodeAdapter(listOf(),this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(EpisodeViewModel::class.java)
        contentRecyclerView.adapter=adapter
        viewModel.getResultList()
                .observe(this, Observer {
                    when(it){
                        is EpisodesData -> {
                            adapter?.updateData(it.results)
                            contentReady()
                        }
                        is EpisodeExhaustionError -> {
                            Snackbar.make(contentRecyclerView,it.errorMessage, Snackbar.LENGTH_SHORT).show()
                            adapter?.exhaustionReached()
                        }
                        is EpisodeUnavailableError -> Snackbar.make(contentRecyclerView,it.errorMessage, Snackbar.LENGTH_SHORT).show()
                    }
                })
    }


    override fun fetchData() {
        viewModel.getMoreData()
    }

    override fun onBottomReached() {
        viewModel.getMoreData()
    }

}