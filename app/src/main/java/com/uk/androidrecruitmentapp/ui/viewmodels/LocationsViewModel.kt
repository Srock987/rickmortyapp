package com.uk.androidrecruitmentapp.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.uk.androidrecruitmentapp.data.local.locations.*
import com.uk.androidrecruitmentapp.data.repositories.LocationRepository
import com.uk.androidrecruitmentapp.data.utils.DataState
import com.uk.androidrecruitmentapp.utils.NextInfoExtractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class LocationsViewModel(repository: LocationRepository) : BaseViewModel<Location>(repository) {

    override fun dataExhausted() {
        updateData(LocationExhaustionError("No more locations"))
    }

    override fun dataUnavailable() {
        updateData(LocationUnavailableError("Can't connect"))
    }

    override fun dataReceived(data: Location) {
        if (data is LocationData) {
            val currentList = getResultList().value
            when (currentList) {
                is LocationData -> updateData(LocationData(data.info,currentList.results.plus(data.results.filter { t -> !currentList.results.contains(t) })))
                is LocationExhaustionError -> updateData(currentList)
                is LocationUnavailableError -> updateData(currentList)
                null -> updateData(data)
            }
            checkExhaustion(data.info!!)
        }
    }

}