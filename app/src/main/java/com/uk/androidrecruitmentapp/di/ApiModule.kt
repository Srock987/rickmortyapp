package com.uk.androidrecruitmentapp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.uk.androidrecruitmentapp.data.remote.ApiService
import com.uk.androidrecruitmentapp.data.repositories.CharactersRepository
import com.uk.androidrecruitmentapp.data.repositories.EpisodeRepository
import com.uk.androidrecruitmentapp.data.repositories.LocationRepository
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.CharactersViewModelFactory
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.EpisodeViewModelFactory
import com.uk.androidrecruitmentapp.ui.viewmodels.factories.LocationsViewModelFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    internal fun providesGson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    internal fun providesOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }

    @Provides
    @Singleton
    internal fun providesGsonConverterFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

    @Provides
    @Singleton
    internal fun providesRx2JavaCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    internal fun providesRetrofit(gsonConverterFactory: GsonConverterFactory, okHttpClient: OkHttpClient,
                                  rxJava2CallAdapterFactory: RxJava2CallAdapterFactory): Retrofit {
        return Retrofit.Builder().addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create<ApiService>(ApiService::class.java)
    }

    @Provides
    @Singleton
    internal fun provideEpisodeRepository(apiService: ApiService): EpisodeRepository{
        return EpisodeRepository(apiService)
    }

    @Provides
    @Singleton
    internal fun provideCharacterRepository(apiService: ApiService): CharactersRepository{
        return CharactersRepository(apiService)
    }

    @Provides
    @Singleton
    internal fun provideLocationRepository(apiService: ApiService): LocationRepository{
        return LocationRepository(apiService)
    }

    @Provides
    @Singleton
    internal fun provideEpisodesViewModelFactory(repository: EpisodeRepository): EpisodeViewModelFactory {
        return EpisodeViewModelFactory(repository)
    }

    @Provides
    @Singleton
    internal fun provideCharactersViewModelFactory(repository: CharactersRepository): CharactersViewModelFactory {
        return CharactersViewModelFactory(repository)
    }

    @Provides
    @Singleton
    internal fun provideLocationsViewModelFactory(repository: LocationRepository): LocationsViewModelFactory {
        return LocationsViewModelFactory(repository)
    }


    companion object {
        private const val BASE_URL = "https://rickandmortyapi.com/api/"
    }
}