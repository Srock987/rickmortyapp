package com.uk.androidrecruitmentapp.di

import com.uk.androidrecruitmentapp.ui.activities.BottomNavigationActivity
import com.uk.androidrecruitmentapp.ui.fragments.CharactersFragment
import com.uk.androidrecruitmentapp.ui.fragments.EpisodeFragment
import com.uk.androidrecruitmentapp.ui.fragments.LocationsFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(fragment: EpisodeFragment)

    fun inject(fragment: CharactersFragment)

    fun inject(fragment: LocationsFragment)

    fun inject(activity: BottomNavigationActivity)
}