package com.uk.androidrecruitmentapp.utils

class NextInfoExtractor {

    companion object {

        private val pageIndicator = "page="

        fun shouldShowNextPage(next: String, pagesCount: Int): Boolean{
            if (next.isNotBlank()){
                return extractPage(next)<=pagesCount
            }
            return false
        }

        fun extractPage(next: String): Int{
            return next.substring(next.lastIndexOf(pageIndicator)+ pageIndicator.length).toInt()
        }

    }
}