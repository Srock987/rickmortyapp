package com.uk.androidrecruitmentapp.utils

enum class NavigationType {
    EPISODES,
    CHARACTERS,
    LOCATIONS
}