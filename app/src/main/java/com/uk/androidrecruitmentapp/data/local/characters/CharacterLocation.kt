package com.uk.androidrecruitmentapp.data.local.characters

data class CharacterLocation(
    val name: String,
    val url: String
)