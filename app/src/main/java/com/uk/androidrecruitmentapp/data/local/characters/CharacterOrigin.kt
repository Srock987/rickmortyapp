package com.uk.androidrecruitmentapp.data.local.characters

data class CharacterOrigin(
        val name: String,
        val url: String
)