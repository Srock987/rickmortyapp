package com.uk.androidrecruitmentapp.data.local.episodes

import com.uk.androidrecruitmentapp.data.local.Info

sealed class Episodes

data class EpisodesData(
        var info: Info? = null,
        val results: List<EpisodeResult> = listOf()
): Episodes()

data class EpisodeExhaustionError(val errorMessage: String): Episodes()

data class EpisodeUnavailableError(val errorMessage: String): Episodes()



