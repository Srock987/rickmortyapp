package com.uk.androidrecruitmentapp.data.repositories

import com.uk.androidrecruitmentapp.data.local.locations.Location
import com.uk.androidrecruitmentapp.data.local.locations.LocationData
import com.uk.androidrecruitmentapp.data.remote.ApiService
import io.reactivex.Single

class LocationRepository(private val apiService: ApiService) : BaseRepository<Location>(apiService) {

    override fun getData(page: Int): Single<Location> {
        return apiService.getMoreLocations(page).map { t: LocationData -> t as Location }
    }

}