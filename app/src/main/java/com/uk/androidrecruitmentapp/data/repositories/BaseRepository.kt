package com.uk.androidrecruitmentapp.data.repositories

import com.uk.androidrecruitmentapp.data.remote.ApiService
import io.reactivex.Single

abstract class  BaseRepository<T>(private val apiService: ApiService) {
    abstract fun getData(page: Int): Single<T>
}