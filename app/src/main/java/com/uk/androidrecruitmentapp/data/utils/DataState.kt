package com.uk.androidrecruitmentapp.data.utils

enum class DataState{
    NORMAL,
    UNAVAILABLE,
    EXHAUSTED
}