package com.uk.androidrecruitmentapp.data.local.characters

import com.uk.androidrecruitmentapp.data.local.Info

sealed class Characters

data class CharactersData(var info: Info? = null,
                          val results: List<CharacterResult> = mutableListOf()): Characters()

data class CharactersExhaustionError(val errorMessage: String): Characters()

data class CharactersUnavailableError(val errorMessage: String): Characters()


