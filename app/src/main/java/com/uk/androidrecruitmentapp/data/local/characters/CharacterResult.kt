package com.uk.androidrecruitmentapp.data.local.characters

data class CharacterResult(
        val id: Int,
        val name: String,
        val status: String,
        val species: String,
        val type: String,
        val gender: String,
        val origin: CharacterOrigin,
        val location: CharacterLocation,
        val image: String,
        val episodes: Array<String>,
        val url: String,
        val created: String
)