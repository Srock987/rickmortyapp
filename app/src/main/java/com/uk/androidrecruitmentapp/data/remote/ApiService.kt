package com.uk.androidrecruitmentapp.data.remote

import com.uk.androidrecruitmentapp.data.local.characters.Characters
import com.uk.androidrecruitmentapp.data.local.characters.CharactersData
import com.uk.androidrecruitmentapp.data.local.episodes.Episodes
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodesData
import com.uk.androidrecruitmentapp.data.local.locations.Location
import com.uk.androidrecruitmentapp.data.local.locations.LocationData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("episode/")
    fun getMoreEpisodes( @Query("page") page: Int): Single<EpisodesData>

    @GET("character/")
    fun getMoreCharacters(@Query("page") page: Int): Single<CharactersData>

    @GET("location/")
    fun getMoreLocations(@Query("page") page: Int): Single<LocationData>

}