package com.uk.androidrecruitmentapp.data.local.locations

import com.uk.androidrecruitmentapp.data.local.Info

sealed class Location

data class LocationData(
        var info: Info? = null,
        val results: List<LocationResult> = listOf()
): Location()

data class LocationExhaustionError(val errorMessage: String): Location()

data class LocationUnavailableError(val errorMessage: String): Location()


