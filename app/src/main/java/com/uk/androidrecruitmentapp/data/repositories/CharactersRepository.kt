package com.uk.androidrecruitmentapp.data.repositories

import android.os.Debug
import android.util.Log
import com.uk.androidrecruitmentapp.data.local.characters.Characters
import com.uk.androidrecruitmentapp.data.local.characters.CharactersData
import com.uk.androidrecruitmentapp.data.remote.ApiService
import io.reactivex.Single

class CharactersRepository(private val apiService: ApiService): BaseRepository<Characters>(apiService) {

    override fun getData(page: Int): Single<Characters> {
        return apiService.getMoreCharacters(page).map { t: CharactersData -> t as Characters }
    }

}