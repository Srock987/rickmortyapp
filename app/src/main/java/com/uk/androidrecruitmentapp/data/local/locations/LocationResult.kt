package com.uk.androidrecruitmentapp.data.local.locations

data class LocationResult(
        val id: Int,
        val name: String,
        val type: String,
        val dimension: String,
        val residents: List<String>,
        val url: String,
        val created: String
)