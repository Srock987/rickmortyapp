package com.uk.androidrecruitmentapp.data.repositories

import com.uk.androidrecruitmentapp.data.local.episodes.Episodes
import com.uk.androidrecruitmentapp.data.local.episodes.EpisodesData
import com.uk.androidrecruitmentapp.data.remote.ApiService
import io.reactivex.Single

class EpisodeRepository(private val apiService: ApiService) : BaseRepository<Episodes>(apiService) {

    override fun getData(page: Int): Single<Episodes> {
        return apiService.getMoreEpisodes(page).map { t: EpisodesData -> t as Episodes }
    }

}